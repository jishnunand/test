Email based login
Token based api access
User Listing

# Login
URL: http://127.0.0.1:8000/api/rest-auth/login/
method: Post
body/form-data: 
	email: asd@asd.com
	password: asd123

response:
	{
    "key": "9c7a1577a76d2e1194eafdaaf9692ca9a798b4cf"
}



# usersList
URL: http://127.0.0.1:8000/api/user-list/
method: get
Header: 
	Key: Authorization 
	Value: Token <token value got from login api>

response:
[
    {
        "id": 1,
        "password": "pbkdf2_sha256$216000$i4IIKNxZnCeF$2f3rDq1VNXdJHdnc0eQBj/NFZAvlWZUzyVRZxDRezLU=",
        "last_login": "",
        "is_superuser": true,
        "email": "jishnunand@gmail.com",
        "is_staff": false,
        "is_active": true,
        "groups": [],
        "user_permissions": []
    },
    {
        "id": 2,
        "password": "pbkdf2_sha256$216000$NmajHpIjdVzY$Pf5enOOHzOUDdMT63Jlv1kkdYMJPhV/UludhqMiIjsg=",
        "last_login": "2020-12-10T09:26:44.208087Z",
        "is_superuser": true,
        "email": "asd@asd.com",
        "is_staff": true,
        "is_active": true,
        "groups": [],
        "user_permissions": []
    }
]