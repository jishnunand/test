import uuid


from django.db import models
from django.conf import settings


# Create your models here.

class Quiz(models.Model):
	object_id = models.UUIDField(
        unique=True, editable=False, verbose_name='Public identifier')
	quiz_name = models.CharField(max_length=255)


class Question(models.Model):
	quiz = models.ForeignKey(
        Quiz, on_delete=models.SET_NULL, blank=True, null=True, )
	question =  models.CharField(max_length=255)


class QuizOptions(models.Model):
	question = models.ForeignKey(
        Question, on_delete=models.SET_NULL, blank=True, null=True)
	options = models.CharField(max_length=255)
	is_correct_answer = models.BooleanField(default=False)


class Response(models.Model):
	user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
	quiz = models.ForeignKey(
        Quiz, on_delete=models.SET_NULL, blank=True, null=True)

class QuestionResponse(models.Model):
    object_id = models.UUIDField(
        unique=True, editable=False, verbose_name='Public identifier')

    question = models.ForeignKey(
        Question, on_delete=models.SET_NULL, blank=True, null=True)
    choice = models.ForeignKey(
        QuizOptions, on_delete=models.SET_NULL, blank=True, null=True)
    response = models.ForeignKey(
        Response, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        db_table = "question_response"

