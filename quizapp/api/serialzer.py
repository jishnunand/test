from rest_framework.serializers import (ModelSerializer, SerializerMethodField,
                                        SlugRelatedField, CharField, ReadOnlyField)


from django.contrib.auth import get_user_model

from quiz.models import Response


User = get_user_model()

class TeamSerializer(ModelSerializer):
    """
    Displaying Team Members in Dictionary Format
    """

    class Meta:
        model = User
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        for key, values in data.items():
            try:
                if data[key] is None:
                    data[key] = ""
            except KeyError:
                pass
        return data


class ResponseSerializer(ModelSerializer):


    class Meta:
        model = Response
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        for key, values in data.items():
            try:
                if data[key] is None:
                    data[key] = ""
            except KeyError:
                pass
        return data