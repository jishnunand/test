from django.shortcuts import render
from rest_framework.generics import (CreateAPIView, DestroyAPIView,
                                     ListAPIView, RetrieveUpdateAPIView,
                                     UpdateAPIView)

from django.contrib.auth import get_user_model

from api.serialzer import TeamSerializer

User = get_user_model()

# Create your views here.
class UserListView(ListAPIView):
    serializer_class = TeamSerializer

    def get_queryset(self, *args, **kwargs):
        queryset_list = User.objects.all()
        return queryset_list
