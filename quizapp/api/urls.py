from django.urls import include, path, re_path
from api import views

urlpatterns = [
	path('rest-auth/', include('rest_auth.urls')),
	path('user-list/', views.UserListView.as_view(),
         name="userlist-api"),
]